# La Voce: analisi/sintesi/risintesi

Materiale per una lezione a quattro mani (Vidolin-Bernardini) sulla voce. (in italiano)

## Programma

### [Giorno 1: teoria/lettura/analisi](./giorno-01/README.md)

### [Giorno 2: sintesi/elaborazione](./giorno-02/README.md)

## LICENZA

<img
  src=http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png
  width=88
  alt="CreativeCommons Attribution-ShareAlike 4.0"
/>
