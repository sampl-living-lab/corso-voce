# Campionamento

## La rappresentazione temporale dei suoni: i sonogrammi.

I `sonogrammi` sono una [rappresentazione della variazione di pressione
dell'aria in funzione del tempo](./sonogramma_totale.jpg)

![sonogramma totale](./sonogramma_totale.jpg)

## Tracciando la funzione che riporta la `variazione di ampiezza` del suono...

... si ottiene [la *funzione d'inviluppo*](./inviluppo_totale.jpg).

![inviluppo totale](./inviluppo_totale.jpg)

## Il sonogramma in dettaglio

![dettaglio sonogramma](./sonogramma_dettaglio.jpg)

## La `rappresentazione digitale` del suono

Nella `rappresentazione digitale` dei suoni, il suono è rappresentato da una
sequenza numerica che rappresenta [misure di pressione in rapida successione](./campionamento.jpg)
(intorno a 40.000 misure al secondo).

![campionamento](./campionamento.jpg)

## La `frequenza di campionamento`

La `frequenza di campionamento` è la frequenza con la quale vengono prese le
misure del segnale audio. Può essere utile confrontare tale frequenza con la
definizione in pixel di una fotografia: all'aumentare del numero dei pixel
aumenta la definizione della fotografia. All'aumentare della frequenza di
campionamento aumenta la *definizione* del suono.

![sotto-campionamento](./sotto-campionamento-1a16.jpg)

# [Codice `GNU Octave` che ha prodotto questi diagrammi](./sonogram.m)

```matlab
clear all; close all;
pkg load signal;
%
% leggere un file audio e disporlo in un vettore n x 1
%

[y fs] = audioread('../suoni/speech-male.wav');
sinc = 1/fs;
samples = length(y);
dur = samples / fs;
t = [0:sinc:dur-sinc]';

figure(1);
plot(t, y);
axis([0 dur -0.6 0.6]);
title('sonogramma');
set(gca, "xgrid", "on");
print("sonogramma_totale.jpg", "-djpeg")

%
% estrazione dell'inviluppo
%
figure(2);
cutoff = 60; % 60 Hz cutoff
buttercutoff = cutoff/fs;
[b, a] = butter(2, buttercutoff);
env = filter(b, a, abs(y));
%
% compensazione d'ampiezza
%
ymax = max(y);
envmax = max(env);
compfactor = ymax/envmax;
env *= compfactor;
plot(t, y, t, env, 'linewidth', 4);
axis([0 dur -0.6 0.6]);
set(gca, "xgrid", "on");
title("inviluppo d'ampiezza");
print("inviluppo_totale.jpg", "-djpeg")

%
% dettaglio
%
figure(3);
detstart = 1.4;
detend   = 1.43;
initper = 1.40159;
finisper = 1.408165;
period = finisper-initper;
finisplot = initper + (period*4);
idxstart = round(detstart*fs);
idxend = round(detend*fs);
xtics = [initper:period:finisplot];
tit = [ 'sonogramma - dettaglio da ' num2str(initper) ' a ' num2str(finisplot) ];
plot(t(idxstart:idxend,1), y(idxstart:idxend,1), 'linewidth', 4);
title(tit)
axis([initper finisplot -0.6 0.6])
set(gca, "xtick", xtics);
set(gca, "xgrid", "on");
print("sonogramma_dettaglio.jpg", "-djpeg")
%
% campionamento
%
figure(4);
nsamples = 130;
idxend2 = idxstart + nsamples;
finisplot2 = t(idxend2);
tit2 = [ 'campioni - dettaglio da ' num2str(initper) ' a ' num2str(finisplot2) ];
subplot(2,1,1)
plot(t(idxstart:idxend2,1), y(idxstart:idxend2,1), 'linewidth', 2, t(idxstart:idxend2,1), y(idxstart:idxend2, 1), '+', 'linewidth', 2);
axis([initper finisplot2 -0.6 0.6])
title(tit2)
subplot(2,1,2)
bar(t(idxstart:idxend2,1), y(idxstart:idxend2,1));
axis([initper finisplot2 -0.6 0.6])
print("campionamento.jpg", "-djpeg")

%
% sotto-campionamento
%
figure(5)
ssrate = 16;
sfs = fs/ssrate;
sry = zeros(size(y));
nsamples = 500;
idxend2 = idxstart + nsamples;
finisplot2 = t(idxend2);
for k=1:ssrate:samples
  sry(k:k+ssrate-1,1) = ones(ssrate,1) * y(k,1);
end
subplot(2,1,1)
bar(t(idxstart:idxend2, 1), y(idxstart:idxend2, 1), 1)
title(["L'effetto del sotto-campionamento (1:" num2str(ssrate) ")"])
axis([initper finisplot2 -0.6 0.6])
subplot(2,1,2)
bar(t(idxstart:idxend2, 1), sry(idxstart:idxend2, 1), 1)
axis([initper finisplot2 -0.6 0.6])
print("sotto-campionamento-1a16.jpg", "-djpeg")
```

# [Patch puredata per gli oscilloscopi](./campionamento.pd)

![screenshot patch puredata](./campionamento_pd_guts.png)
