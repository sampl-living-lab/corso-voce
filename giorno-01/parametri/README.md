# Parametri dei suoni

## I parametri indipendenti che caratterizzano un suono sono sostanzialmente: `frequenza`, `ampiezza`, `forma d'onda`

### La Frequenza

La `frequenza` è il numero di ripetizioni dello stesso ciclo in un'unità di
tempo. Quando l'unità di tempo è il secondo, l'unità di misura della frequenza
è il `Hertz`, ossia il numero di cicli per secondo.

Per sua definizione essa è l'inverso del `periodo` - ossia il tempo impiegato
per completare un ciclo. 

![periodo](./frequenza.jpg)

In linea generale la frequenza è legata all'altezza percepita.

### L'Ampiezza

L'`ampiezza` del suono è, nel grafico, l'estensione verticale della variazione
di pressione dell'aria.

![ampiezza](./ampiezza.jpg)

L'ampiezza è legata all'intensità percepita.

Considerando la sensibilità dell'orecchio ci sono due tipi di rappresentazione
dell'ampiezza:

* la `rappresentazione microscopica` è una rappresentazione lineare
  (generalmente tra due valori arbitrari, ad es. tra `-1` e `+1`)
* la `rappresentazione macroscopica` è una rappresentazione logaritimica la
  cui unità di misura è il `deciBel` (`dB`) che è una misura *differenziale*.

### La Forma d'Onda

La `forma d'onda` ha bisogno di una spiegazione più approfondita in un
capitolo separato.

Essa è - in forma estremamente generica - legata al `timbro` percepito.

# [Codice `GNU Octave` che ha prodotto questi diagrammi](./parametri.m)

```matlab
%
% annotazione dei parametri
%
clear all; close all;
pkg load signal;
%
% leggere un file audio e disporlo in un vettore n x 1
%

[y fs] = audioread('../suoni/speech-male.wav');
sinc = 1/fs;
samples = length(y);
dur = samples / fs;
t = [0:sinc:dur-sinc]';

%
% frequenza
%
figure(1);
detstart = 1.4;
detend   = 1.43;
initper = 1.40159;
finisper = 1.408165;
period = finisper-initper;
finisplot = initper + (period*4);
idxstart = round(detstart*fs);
idxend = round(detend*fs);
xtics = [initper:period:finisplot];
tit = [ 'sonogramma - dettaglio da ' num2str(initper) ' a ' num2str(finisplot) ];
plot(t(idxstart:idxend,1), y(idxstart:idxend,1), 'linewidth', 4);
title(tit)
axis([initper finisplot -0.6 0.6])
set(gca, "xtick", xtics);
set(gca, "xgrid", "on");
text(1.40817, 0.4, "inizio periodo");
text(1.4148, 0.4, "fine periodo");
print("frequenza.jpg", "-djpeg")
%
% ampiezza
%
figure(2);
short_y = y(idxstart:idxend, 1);
symax = max(short_y);
symin = min(short_y);
ytics = [symin symax];
tit = [ 'sonogramma - dettaglio da ' num2str(initper) ' a ' num2str(finisplot) ];
plot(t(idxstart:idxend,1), short_y, 'linewidth', 4);
title(tit)
axis([initper finisplot -0.6 0.6])
set(gca, "ytick", ytics);
set(gca, "ygrid", "on");
print("ampiezza.jpg", "-djpeg")
```

# [Patch puredata per i parametri](./parametri.pd)

![screenshot patch puredata](./parametri_pd_guts.png)
