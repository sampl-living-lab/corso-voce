%
% annotazione dei parametri
%
clear all; close all;
pkg load signal;
%
% leggere un file audio e disporlo in un vettore n x 1
%

[y fs] = audioread('../suoni/speech-male.wav');
sinc = 1/fs;
samples = length(y);
dur = samples / fs;
t = [0:sinc:dur-sinc]';

%
% frequenza
%
figure(1);
detstart = 1.4;
detend   = 1.43;
initper = 1.40159;
finisper = 1.408165;
period = finisper-initper;
finisplot = initper + (period*4);
idxstart = round(detstart*fs);
idxend = round(detend*fs);
xtics = [initper:period:finisplot];
tit = [ 'sonogramma - dettaglio da ' num2str(initper) ' a ' num2str(finisplot) ];
plot(t(idxstart:idxend,1), y(idxstart:idxend,1), 'linewidth', 4);
title(tit)
axis([initper finisplot -0.6 0.6])
set(gca, "xtick", xtics);
set(gca, "xgrid", "on");
text(1.40817, 0.4, "inizio periodo");
text(1.4148, 0.4, "fine periodo");
print("frequenza.jpg", "-djpeg")
%
% ampiezza
%
figure(2);
short_y = y(idxstart:idxend, 1);
symax = max(short_y);
symin = min(short_y);
ytics = [symin 0 symax];
tit = [ 'sonogramma - dettaglio da ' num2str(initper) ' a ' num2str(finisplot) ];
plot(t(idxstart:idxend,1), short_y, 'linewidth', 4);
title(tit)
axis([initper finisplot -0.6 0.6])
set(gca, "ytick", ytics);
set(gca, "ygrid", "on");
print("ampiezza.jpg", "-djpeg")
