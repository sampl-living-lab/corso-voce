# Corso Voce: programma giorno 1

## Prima parte

### Rappresentazione temporale

* [campionamento](./campionamento/README.md)
  * rappresentazione temporale (sonogramma)
  * definizione temporale
    * definizione
    * ricampionamento -> legame tempo/frequenza
* [parametri](./parametri/README.md):
  * frequenza
    * cicli per secondo
  * ampiezza
    * rappresentazione fine (lineare)
    * rappresentazione macro (logaritmica - misura in dB)
  * timbro (forma)

### Rappresentazione spettrale

* [Rappresentazione spettrale discreta](./DFT/README.md)
  * compromesso tempo/frequenza
  * armonicità/inarmonicità
  * Rappresentazione spettrale a breve termine

## Seconda parte: laboratorio

* introduzione a `pure data`
* introduzione a `octave`
