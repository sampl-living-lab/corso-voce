# La Rappresentazione `spettrale` dei suoni

Com'è possibile *quantificare una forma d'onda*?

Una forma d'onda qualsiasi può essere scomposta in una serie di componenti
semplici (generalmente sinusoidi o cosinusoidi - dette `componenti parziali`) che vengono sommate insieme
seguendo regole specifiche per ogni forma. Questa operazione di
*scomposizione* si chiama *scomposizione in serie di Fourier* e può essere
realizzata anche al contrario, ossia *componendo* una forma d'onda sommando le
componenti parziali (`scomposizione inversa`).

Se le componenti parziali sono di frequenza multipla di una `frequenza
fondamentale` (i.e. la frequenza della prima parziale), allora tali componenti
si chiameranno `armoniche` e il suono avrà un'altezza facilmente percepibile.

## Esempio 1: Costruzione di un'onda *dente di sega*

La regola di costruzione di una onda *dente di sega* consiste nel utilizzare
solo sinusoidi armoniche con ampiezza `1/n`.

La somma algebrica delle prime due componenti restituisce:

![Due componenti](./due_componenti.jpg)

La somma algebrica delle prime dieci componenti restituisce:

![Dieci componenti](./dieci_componenti.jpg)

La somma algebrica delle prime cento componenti restituisce:

![Cento componenti](./cento_componenti.jpg)

## Esempio 2: scomposizione (discreta) in serie di Fourier di un suono esistente (spettrogramma)

![Rappresentazione spettrale](./spettrogramma.jpg)

## Compromesso tempo/frequenza

I suoni reali sono `pseudo-periodici`, ossia sono un misto di periodicità e di
aperiodicità (i.e.: cambiano nel tempo). Per questo tipo di funzioni è
opportuno fare scomposizioni in serie di Fourier per piccole porzioni
temporali.

![waterfall plot](./waterfall.jpg)

# [Codice `GNU Octave` che ha prodotto questi diagrammi](./cv_dft.m)

```matlab
clear all;
close all;
fs = 48000;
sinc = 1/fs;
fzero = 65.15;
pzero = 1/fzero;
nsamples = pzero * fs;
dur = pzero;
t = [0:sinc:dur-sinc];

figure(1)

amp0 = 1;
comp0 = amp0 * sin(2*pi*fzero*t);
amp1 = 1/2;
comp1 = amp1 * sin(2*pi*fzero*2*t);

plot(t, comp0, '-;prima componente;', t, comp1, '-;seconda componente;', t, comp0 .+ comp1, '-;somma delle componenti;', 'linewidth', 3)
title("somma delle prime due componenti");
axis([0 dur]);
print("due_componenti.jpg", "-djpeg")

figure(2)

ncomp = 10;

wsum = zeros(1, nsamples);
hold on;
for k=1:ncomp
  ampn = 1/k;
  frqn = fzero*k;
  comp = ampn*sin(2*pi*frqn*t);
  wsum .+= comp;
  plot(t, comp);
end
plot(t, wsum, 'linewidth', 3);
hold off;
title(['somma delle prime ' num2str(ncomp) ' componenti']);
axis([0 dur]);
print("dieci_componenti.jpg", "-djpeg")

figure(3)

ncomp = 100;

wsum = zeros(1, nsamples);
for k=1:ncomp
  ampn = 1/k;
  frqn = fzero*k;
  comp = ampn*sin(2*pi*frqn*t);
  wsum .+= comp;
end
plot(t, wsum, 'linewidth', 3);
title(['somma delle prime ' num2str(ncomp) ' componenti']);
axis([0 dur]);
print("cento_componenti.jpg", "-djpeg")

figure(4)

%
% leggere un file audio e disporlo in un vettore n x 1
%

[y fs] = audioread('../suoni/speech-male.wav');
sinc = 1/fs;
initper = 1.40159;
finisper = 1.408165;
period = finisper-initper;
finisplot = initper + (period*8);
initsamp = round(initper*fs);
endsamp = round(finisplot*fs);
y = y(initsamp:endsamp, 1);
samples = length(y);
dur = samples / fs;
t = [0:sinc:dur-sinc]';

winsize = 2048;
binsize = fs/winsize;
f = [0:binsize:fs-binsize];
h = hanning(winsize);
ywin = y(1:winsize) .* h;
ydft = 2*abs(fft(ywin, winsize))/winsize;
subplot(2,1,1)
plot(t, y, 'linewidth', 3)
title('sonogramma')
subplot(2,1,2)
plot(f, ydft, 'linewidth', 3);
axis([0 fs/16])
title('spettrogramma')
print("spettrogramma.jpg", "-djpeg")

figure(5)

%
% leggere un file audio e disporlo in un vettore n x 1
%

[y fs] = audioread('../suoni/speech-male.wav');
sinc = 1/fs;
samples = length(y);
dur = samples / fs;
t = [0:sinc:dur-sinc]';

winsize = 128;
binsize = fs/winsize;
overlap = 4;
hopsize = winsize/overlap;
hopsize_sinc = hopsize * sinc;
f = [0:binsize:fs-binsize];
t_stft = [0:hopsize_sinc:dur-hopsize_sinc];
[ystft, parms] = stft(y, winsize, hopsize);

mesh(t_stft(1:size(ystft,2)), f, 20*log10(2*abs(ystft)/winsize));
axis([0 dur 0 fs/16 -100 -28]);
limits = axis();
print("waterfall.jpg", "-djpeg")
```
