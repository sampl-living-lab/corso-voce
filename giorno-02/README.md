# Corso Voce: programma giorno 2

### Manipolazione del segnale vocale

* ricapitolazione e riassunto della prima giornata
* funzionamento del `vocoder`
* problematiche dello spostamento in frequenza
* stretching/shrinking
* agogica: portamento, vibrato, glissato

#### Esempi musicali


### Laboratorio

* oscillatore con vibrato e tremolo regolabili
* `vocoder`
* spostamento dei formanti
* `harmonizer`
